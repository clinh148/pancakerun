﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Disc : MonoBehaviour
{
    private Rigidbody rb;
    public LayerMask layerMask;
    private bool isPhysic = false;
    public List<FoodStack> _stacks;
    private Transform curBody;
    private Transform preBody;
    private bool isColFinish = false;
    public void ChangeSkin(int id)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        GameObject g = Instantiate(ResourceManager.I.skinDTOs[id].objSkin);
        g.transform.SetParent(transform);
        g.transform.localScale = Vector3.one;
    }
    private void Start()
    {
        if(rb == null)
        {
            rb = GetComponent<Rigidbody>();
        }
        ChangeSkin(DataManager.I.GameData.idSkin);
    }
    public void DropFoodFinish(int size, float y)
    {
        for (int i = size - 1; i >= 0; i--)
        {
            if (_stacks[i])
            {
                var rb = _stacks[i].GetComponent<Rigidbody>();
                _stacks[i].isColObs = true;
                rb.isKinematic = true;
                _stacks[i].transform.SetParent(null);
                _stacks[i].transform.DOMoveY(y + (i * 0.2f), 0.1f);
                _stacks.RemoveAt(i);
            }
        }
        SoundManager.I.PlaySound(TypeSound.stairstep);
    }
    Vector3 posCamFinish;
    float sFOVCam;
    float eFOVCam = 80;
    Tween twFOVC;
    float sXCam;
    float eXCam;
    Tween twXCam;
    public void CamFollow()
    {
        if (twFOVC != null)
            twFOVC.Kill(false);
        sFOVCam = Camera.main.fieldOfView;
        eFOVCam = (80 + GameManager.I.disc._stacks.Count * 0.25f);
        twFOVC = DOTween.To(() => sFOVCam, k => eFOVCam = k, eFOVCam, 0.5f).OnUpdate(() =>
        {
            Camera.main.fieldOfView = eFOVCam;
        });
        //
        /*if (twXCam != null)
            twXCam.Kill(false);
        sXCam = Camera.main.transform.eulerAngles.x;
        eXCam = 10 - ((eFOVCam - 80) / 2f);
        twXCam = DOTween.To(() => sXCam, k => eXCam = k, eXCam, 0.1f).OnUpdate(() =>
        {
            Camera.main.transform.eulerAngles = new Vector3(eXCam,transform.eulerAngles.y,transform.eulerAngles.z);
        });*/
        //
    }
    IEnumerator IEMoveFinish()
    {
        float timeStep = 0.3f;
        //float zIncreaseDiscRoadBonus = 2f;
        var f = GameManager.I.finish;
        var d = GameManager.I.disc;
        Camera.main.transform.GetComponent<FollowObject>().enabled = false;
        var n = d._stacks.Count / GameConfig.AMOUNT_FOOD_STAIR;
        var m = d._stacks.Count % GameConfig.AMOUNT_FOOD_STAIR;
        int l;
        if (m > 0)
            l = n + 1;
        else
            l = n;
        //Debug.Log(n);
        /*if(n >= f.amountStair)
        {
            Debug.Log("bonus:" + n);
        }
        else
        {
            Debug.Log("k bonus:" + n);
        }*/
        for (int i = 0; i < l; i++)
        {
            float t = timeStep;
            if (i == 0)
            {
                t = timeStep + 0.5f;
            }
            if (i == 1)
            {
                posCamFinish = Camera.main.transform.position;
            }
            Vector3 p = f.stairs[i].transform.GetChild(0).position;
            Vector3 s = f.stairs[i].transform.GetChild(0).position;
            Vector3 c = f.stairs[i].transform.GetChild(0).position;
            c.y += 5.5f;
            //Debug.Log(f.transform.localEulerAngles.y);
            if(f.transform.localEulerAngles.y == 90)
                c.x -= GameConfig.DIS_CAM;
            else if (f.transform.localEulerAngles.y == 270)
                c.x += GameConfig.DIS_CAM;
            else
                c.z -= GameConfig.DIS_CAM;
            p.y = transform.position.y;
            Camera.main.transform.DOMove(c, t).SetEase(Ease.Linear);
            transform.DOMove(p, t).SetEase(Ease.Linear).OnComplete(() =>
            {
                /*if(i == 0)
                {
                    transform.GetComponent<MeshRenderer>().enabled = false;
                }*/
                DropFoodFinish(GameConfig.AMOUNT_FOOD_STAIR, s.y);
                GameManager.I.moneyCollect += GameConfig.MONEY_COLLECT_STAIR;
                DataManager.I.GameData.money+= GameConfig.MONEY_COLLECT_STAIR;
                DataManager.I.SaveData();
                ManagerUI.I.DisplayMoney();
            });
            yield return new WaitForSeconds(t + 0.01f);
        }

        if (m > 0)
        {
            Vector3 k = f.stairs[l-1].transform.GetChild(0).position;
            for (int i = 0; i < d._stacks.Count; i++)
            {
                d._stacks[i].transform.SetParent(null);
            }
            DropFoodFinish(d._stacks.Count, k.y);
            GameManager.I.moneyCollect += GameConfig.MONEY_COLLECT_STAIR;
            DataManager.I.GameData.money += GameConfig.MONEY_COLLECT_STAIR;
            DataManager.I.SaveData();
            ManagerUI.I.DisplayMoney();
        }
        Run.After(0.3f, () =>
        {
            if(n > 0)
                Camera.main.transform.DOMove(posCamFinish, 1f);
            Run.After(0.5f, () =>
            {
                GameManager.I.GameWin();
            });
        });
        
        /*Vector3 pos = f.objRoadBonus.transform.position;
        pos.z += zIncreaseDiscRoadBonus;

        transform.GetComponent<MeshRenderer>().enabled = true;
        for (int i = 0; i < d._stacks.Count; i++)
        {
            d._stacks[i].transform.SetParent(null);
        }
        Vector3 posT = f.objRoadBonus.transform.position;
        posT.y += 0.1f;
        posT.z += zIncreaseDiscRoadBonus;
        transform.position = posT;
        var y = pos.y + 0.1f;
        Vector3 posC = f.objRoadBonus.transform.position;
        posC.y += 5.5f;
        posC.z -= (GameConfig.DIS_CAM - zIncreaseDiscRoadBonus);
        Camera.main.transform.DOMove(posC, 1f).SetEase(Ease.Linear);
        for (int i = 0; i < d._stacks.Count; i++)
        {
            pos.y = y + i * 0.18f;
            d._stacks[i].transform.DOJump(pos, 0.2f, 1, 1f);
        }
        float v = transform.localScale.z;
        Debug.Log(v);
        transform.localScale = Vector3.zero;
        transform.DOScale(v, 1f).OnComplete(() =>
        {
            for (int i = 0; i < d._stacks.Count; i++)
            {
                d._stacks[i].transform.SetParent(transform);
            }
            Run.After(0.5f, () =>
            {
                transform.SetParent(f.objRoadBonus.transform);
                transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
                GameManager.I.pathFollower.isControlBonus = true;
                float t = Vector3.Distance(transform.position, f.posEndBonus.position) / GameManager.I.pathFollower.speed;
                Vector3 cc = f.posEndBonus.position;
                cc.y += 5.5f;
                cc.z -= GameConfig.DIS_CAM;
                Camera.main.transform.DOMove(cc, t).SetEase(Ease.Linear);
                //transform.DOMove(f.posEndBonus.position, t).SetEase(Ease.Linear);
            });
        });*/
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("OnPhysic"))
        {
            rb.isKinematic = false;
            isPhysic = true;
        }
        else if (other.gameObject.CompareTag("OffPhysic"))
        {
            rb.isKinematic = true;
            isPhysic = false;
            /*Vector3 pos = GameManager.I.pathFollower.pathCreator.path.GetDirectionAtDistance(GameManager.I.pathFollower.distanceTravelled, 
                GameManager.I.pathFollower.endOfPathInstruction);
            transform.position = new Vector3(transform.position.x,pos.y, transform.position.z);*/
        }
        else if (other.gameObject.CompareTag("Finish") && !isColFinish)
        {
            isColFinish = true;
            if(_stacks.Count <= 0)
            {
                GameManager.I.GameLose();
                return;
            }
            rb.isKinematic = true;
            isPhysic = false;
            transform.SetParent(null);
            GameManager.I.pathFollower.isControl = false;
            var f = GameManager.I.finish;
            var d = GameManager.I.disc;
            SoundManager.I.PlaySound(TypeSound.finish);
            if (DataManager.I.GameData.IsHapic)
                Taptic.Light();
            StartCoroutine(IEMoveFinish());
        }
        else if (other.gameObject.CompareTag("Hole"))
        {
            GameManager.I.GameLose();
        }
        else if (other.gameObject.CompareTag("ObsDoor"))
        {
            var d = other.gameObject.GetComponentInParent<ObsDoor>();
            if (d.isDropAll)
            {
                DropFoodAll(other.gameObject.transform.position);
            }
            else
            {
                if(_stacks.Count <= d.sizeDrop)
                {
                    DropFoodAll(other.gameObject.transform.position);
                    GameManager.I.GameLose();
                }
                else
                {
                    GameManager.I.pathFollower.distanceTravelled += 2f;
                    DropFoodDoor(other.gameObject.transform.position,d.sizeDrop);
                }
            }
        }
        else if (other.gameObject.CompareTag("FoodCollect") && !other.gameObject.GetComponent<FoodCollect>().isOnStack)
        {
            SoundManager.I.PlaySound(TypeSound.collect);
            if (DataManager.I.GameData.IsHapic)
                Taptic.Light();
            FoodCollect f = other.gameObject.GetComponent<FoodCollect>();
            f.isOnStack = true;
            f.OffAnim();
            other.gameObject.transform.SetParent(transform);
            var x = Random.Range(-0.015f,0.015f);
            var z = Random.Range(-0.015f,0.015f);
            GameObject g = Instantiate(f.foodStack);
            g.SetActive(false);
            _stacks.Add(g.GetComponent<FoodStack>());
            Vector3 pos = new Vector3(x, (_stacks.Count - 1) * 0.05f + 0.05f, z);

            other.gameObject.transform.localPosition = pos;
           
            other.gameObject.transform.DOLocalJump(pos, 0.25f, 1, 0.4f).SetEase(Ease.Linear).OnComplete(()=>
            {
                CamFollow();
                g.SetActive(true);
                g.transform.SetParent(transform);
                g.transform.eulerAngles = new Vector3(0,Random.Range(0,180),0);
                Vector3 valueSale = g.transform.localScale;
                g.transform.localPosition = pos;
                if (!f.isFoodIntact)
                {
                    g.transform.localScale = Vector3.zero;
                    g.transform.DOScale(valueSale, 0.5f).SetEase(Ease.OutBack);
                }
                else
                {
                    g.transform.localScale = valueSale;
                }
                Destroy(other.gameObject);
            });
            //other.gameObject.GetComponent<SpringJoint>().connectedBody = _stacks[_stacks.Count - 2].GetComponent<Rigidbody>();
        }
    }
    public void DropFoodCustom(Vector3 posCol, float y)
    {
        if(_stacks.Count >0)
            SoundManager.I.PlaySound(TypeSound.obs);
        //
        for (int i = _stacks.Count - 1; i >= 0; i--)
        {
            //Debug.Log(_stacks[i].transform.localPosition.y +"---"+y);
            if(_stacks[i].transform.localPosition.y >= y)
            {
                var rb = _stacks[i].GetComponent<Rigidbody>();
                _stacks[i].isColObs = true;
                rb.isKinematic = false;
                posCol.y = _stacks[i].transform.position.y;
                Vector3 dir = transform.forward;//posCol - _stacks[i].transform.position;
                dir.Normalize();
                dir.y = -2f/2;
                rb.AddForce(-dir * Random.Range(50f, 100f)*2);
                _stacks[i].DestroyFoodStack();
                _stacks.RemoveAt(i);
                CamFollow();
            }
        }
        for (int i = 0; i < _stacks.Count; i++)
        {
            //_stacks[i].transform.localPosition = new Vector3(_stacks[i].transform.localPosition.x, i * 0.07f ,_stacks[i].transform.localPosition.z);
            _stacks[i].transform.DOLocalMoveY(i * 0.05f +0.05f,0.3f).SetEase(Ease.Linear);
        }
    }
    public void DropFoodDoor(Vector3 posCol, int size)
    {
        if (_stacks.Count > 0)
            SoundManager.I.PlaySound(TypeSound.obs);
        for (int i = size - 1; i >= 0; i--)
        {
            var rb = _stacks[i].GetComponent<Rigidbody>();
            _stacks[i].isColObs = true;
            rb.isKinematic = false;
            posCol.y = _stacks[i].transform.position.y;
            Vector3 dir = transform.forward;//posCol - _stacks[i].transform.position;
            dir.Normalize();
            dir.y = -2f / 2;
            rb.AddForce(-dir * Random.Range(50f, 100f) * 2);
            _stacks[i].DestroyFoodStack();
            _stacks.RemoveAt(i);
            CamFollow();
        }
        
        Run.After(0.1f,()=>
        {
            
            for (int i = 0; i < _stacks.Count; i++)
            {
                //_stacks[i].transform.localPosition = new Vector3(_stacks[i].transform.localPosition.x, i * 0.07f ,_stacks[i].transform.localPosition.z);
                _stacks[i].transform.DOLocalMoveY(i * 0.05f+0.05f, 0.3f).SetEase(Ease.Linear);
            }
        });
    }
    public void DropFoodAll(Vector3 posCol)
    {
        if (_stacks.Count > 0)
            SoundManager.I.PlaySound(TypeSound.obs);
        for (int i = _stacks.Count - 1; i >= 0; i--)
        {
            var rb = _stacks[i].GetComponent<Rigidbody>();
            _stacks[i].isColObs = true;
            rb.isKinematic = false;
            posCol.y = _stacks[i].transform.position.y;
            Vector3 dir = transform.forward;//posCol - _stacks[i].transform.position;
            dir.Normalize();
            dir.y = -2f / 2;
            rb.AddForce(-dir * Random.Range(50f, 100f) * 2);
            _stacks[i].DestroyFoodStack();
            _stacks.RemoveAt(i);
            CamFollow();
        }
        /*for (int i = 0; i < _stacks.Count; i++)
        {
            //_stacks[i].transform.localPosition = new Vector3(_stacks[i].transform.localPosition.x, i * 0.07f ,_stacks[i].transform.localPosition.z);
            _stacks[i].transform.DOLocalMoveY(i * 0.07f, 0.3f).SetEase(Ease.Linear);
        }*/
    }
    private float dis;
    /*private void MoveBody()
    {
        for (int i = 1; i < _stacks.Count; i++)
        {
            curBody = _stacks[i].transform;
            preBody = _stacks[i - 1].transform;
            dis = Vector3.Distance(preBody.position, curBody.position);
            Vector3 newPos = preBody.position;
            newPos.y = curBody.position.y;
            //newPos.x = Random.Range(-0.05f, 0.05f);
            //newPos.y = newPos.y + 0.2f;
            //curBody.position = newPos;
            //curBody.position = Vector3.Lerp(curBody.position, newPos, 1f);
            float T = Time.deltaTime * dis / 0.25f;
            if (T > 0.5f)
                T = 0.5f;
            //curBody.position = Vector3.Lerp(curBody.position, newPos, 0.5f);
            //curBody.rotation = Quaternion.Slerp(curBody.rotation, preBody.rotation, 0.5f);
        }
    }*/
}

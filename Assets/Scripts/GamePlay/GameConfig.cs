﻿public class GameConfig 
{
    public static long MONEY_UNLOCK_RANDOM = 1250;
    public static long MONEY_COLLECT_STAIR = 20;
    public static float SPEED_MOVE = 7;
    public static float DIS_CAM = 7f;
    public static int AMOUNT_FOOD_STAIR = 7;
}

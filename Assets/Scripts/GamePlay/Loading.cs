﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Loading : MonoBehaviour
{
    private float timeAnim = 0.3f;
    public TextMeshProUGUI tmpLoading;
    void Start()
    {
        /*Run.After(1f, ()=>
        {
            ManagerUI.I.LoadingStart();
        });*/
        StartCoroutine(IELoading());
    }
    IEnumerator IELoading()
    {
        yield return new WaitForSeconds(timeAnim);
        tmpLoading.text = "Loading.";
        yield return new WaitForSeconds(timeAnim);
        tmpLoading.text = "Loading..";
        yield return new WaitForSeconds(timeAnim);
        tmpLoading.text = "Loading...";
        yield return new WaitForSeconds(timeAnim);
        tmpLoading.text = "Loading..";
        yield return new WaitForSeconds(timeAnim);
        tmpLoading.text = "Loading..";
        yield return new WaitForSeconds(timeAnim);
        tmpLoading.text = "Loading...";
        ManagerUI.I.LoadingStart();
    }
}

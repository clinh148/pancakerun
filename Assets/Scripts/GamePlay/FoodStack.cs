﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodStack : MonoBehaviour
{
    public bool isColObs = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obs") && !isColObs)
        {
            isColObs = true;
            //Debug.Log(transform.localPosition.y);
            if (other.gameObject.GetComponent<Obs>().isDropAll)
                GameManager.I.disc.DropFoodAll(other.gameObject.transform.position);
            else
                GameManager.I.disc.DropFoodCustom(other.gameObject.transform.position, transform.localPosition.y);
        }
    }
    private Coroutine c;
    IEnumerator IEDestroyFoodStack()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }
    public void DestroyFoodStack()
    {
        /*if(c == null)
            c = StartCoroutine(IEDestroyFoodStack());*/
        Run.After(3f,()=>
        {
            if(gameObject)
                Destroy(gameObject);
        });
    }
    /*private void OnDestroy()
    {
        if (c != null)
            StopCoroutine(c);
    }*/
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObsDoor : MonoBehaviour
{
    public bool isDropAll = false;
    public int sizeDrop = 5;
    public bool isMove = false;
    public Transform door;
    private float timeMove = 1.2f;
    private void Start()
    {
        if (isMove)
        {
            MoveUp();
        }
    }
    private void MoveDown()
    {
        door.DOLocalMoveY(-1.4f, timeMove).SetEase(Ease.Linear).OnComplete(MoveUp);
    }
    private void MoveUp()
    {
        door.DOLocalMoveY(0.5f, timeMove).SetEase(Ease.Linear).SetDelay(2f).OnComplete(MoveDown);
    }
}

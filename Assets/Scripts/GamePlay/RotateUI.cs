using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class RotateUI : MonoBehaviour
{
    public Vector3 dir;
    public float speed = 3f;
    private void Update()
    {
        transform.Rotate(dir * speed);
    }
}

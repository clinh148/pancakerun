﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    public int amountStair;
    public GameObject objRoadBonus;
    public Transform posEndBonus;
    public List<GameObject> stairs;
    private float stepY = 1.5f;
    private float stepZ = 2.25f;
    private void Start()
    {
        for (int i = 0; i < stairs.Count; i++)
        {
            if (i < amountStair)
                stairs[i].SetActive(true);
            else
                stairs[i].SetActive(false);
        }
        //
        //objRoadBonus.SetActive(true);
        //objRoadBonus.transform.localPosition = new Vector3(0, 3.35f + (amountStair-1) * stepY,0 + (amountStair -1) * stepZ);
    }
}

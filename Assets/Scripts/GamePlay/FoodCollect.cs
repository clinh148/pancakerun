﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FoodCollect : MonoBehaviour
{
    [HideInInspector]
    public bool isOnStack = false;
    public bool isFoodIntact = false;
    public bool isAnim = false;
    private bool isScale = false;
    public GameObject foodStack;
    private Vector3 valueScaleOrigin;
    private Vector3 valueScale;
    private Tween twScale;
    private void Start()
    {
        valueScaleOrigin = transform.localScale;
        valueScale = transform.localScale - transform.localScale * 15f / 100f;
    }
    private void Update()
    {
        if (isAnim)
        {
            transform.Rotate(Vector3.up * 1f);
            if (!isScale)
            {
                isScale = true;
                twScale = transform.DOScale(valueScale, 1f).SetEase(Ease.Linear).SetLoops(-1,LoopType.Yoyo);
            }
        }
    }
    public void OffAnim()
    {
        transform.eulerAngles = Vector3.zero;
        transform.localScale = valueScaleOrigin;
        if (twScale != null)
            twScale.Kill(false);
    }
}

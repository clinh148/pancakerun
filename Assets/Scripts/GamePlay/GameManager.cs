﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using PathCreation.Examples;
using Game;

public class GameManager : Singleton<GameManager>
{
    public TypeStateGame stateGame;
    public PathFollower pathFollower;
    public Disc disc;
    public Finish finish;
    public ParticleSystem particleWin;
    public long moneyCollect;
    protected override void Awake()
    {
        base.Awake();
        Application.targetFrameRate = 60;
    }
    public void LoadLevel()
    {
        ManagerUI.I.Hide(TypePanelUI.PanelInGame);
        ManagerUI.I.Hide(TypePanelUI.PanelGameOver);
        ManagerUI.I.Hide(TypePanelUI.PanelGameWin);
        ManagerUI.I.Show(TypePanelUI.PanelMain);
    }
    void Start()
    {
        //LoadLevel();
        stateGame = TypeStateGame.Playing;
        moneyCollect = 0;
    }
    private bool isPlay = false;
    public void PlayGame()
    {
        if (!isPlay)
        {
            ManagerUI.I.Hide(TypePanelUI.PanelMain);
            ManagerUI.I.GetPanel<PanelMain>(TypePanelUI.PanelMain).HideAllSetting();
            ManagerUI.I.Show(TypePanelUI.PanelInGame);
            isPlay = true;
        }
        //stateGame = TypeStateGame.Playing;
    }
    private System.TimeSpan interval = new System.TimeSpan(0, 0, 30);
    public void Restart()
    {
        var d = System.DateTime.Now - ManagerUI.I.timeShowInter;
        //Debug.Log(d);
        if (d >= interval)
        {
            //Debug.Log("showinter");
            Game.AdsManager.ShowInterstitial(ManagerUI.I.LoadLevel);
            ManagerUI.I.timeShowInter = System.DateTime.Now;
        }
        else
            ManagerUI.I.LoadLevel();
    }
    public void GameWin()
    {
        if (stateGame == TypeStateGame.GameWin || stateGame == TypeStateGame.GameOver)
            return;
        stateGame = TypeStateGame.GameWin;
        particleWin.Play();
        SoundManager.I.PlaySound(TypeSound.win);
        if (DataManager.I.GameData.IsHapic)
            Taptic.Medium();
        Run.After(1.5f, () =>
        {
            DataManager.I.GameData.level++;
            DataManager.I.SaveData();
            //DefaultAnalytics.LevelCompleted();
            ManagerUI.I.Hide(TypePanelUI.PanelInGame);
            ManagerUI.I.Show(TypePanelUI.PanelGameWin);
        });
    }
    public void GameLose()
    {
        if (stateGame == TypeStateGame.GameWin || stateGame == TypeStateGame.GameOver)
            return;
        stateGame = TypeStateGame.GameOver;
        SoundManager.I.PlaySound(TypeSound.lose);
        pathFollower.speed = 0;
        var rb = disc.GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.None;
        rb.isKinematic = false;
        //DefaultAnalytics.LevelFailed();
        //Debug.Log("lose");
        Run.After(1,()=>
        {
            ManagerUI.I.Hide(TypePanelUI.PanelInGame);
            ManagerUI.I.Show(TypePanelUI.PanelGameOver);
        });
    }
}

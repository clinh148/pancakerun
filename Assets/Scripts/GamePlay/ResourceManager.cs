using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class SkinDTO
{
    public int idSkin;
    public GameObject objSkin;
    public Sprite sprSkin;
    public bool isUnlock = false;
}

public class ResourceManager : Singleton<ResourceManager>
{
    public List<SkinDTO> skinDTOs;
}

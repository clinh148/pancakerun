using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSkin : MonoBehaviour
{
    public int id;
    public GameObject imgBoder;
    public Image imgSkin;
    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(Click);
    }
    public void Click()
    {
        DataManager.I.GameData.idSkin = id;
        DataManager.I.SaveData();
        var p = ManagerUI.I.GetPanel<PanelShop>(TypePanelUI.PanelShop);
        p.SelectSkinByID(id);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Rotate : MonoBehaviour
{
    public bool isRotate = false;
    public bool isMove = false;
    private float speedRotate = 5f;
    private float speedMove = 2f;
    public Vector3 dir;
    public GameObject objLineBlade;
    public float posXStart = -1f;
    public float posXEnd = 1f;
    public float timeDelay = 0f;
    private void Start()
    {
    }
    void Update()
    {
        if (isMove)
        {
            isMove = false;

            GameObject g = Instantiate(objLineBlade);
            float sizeX = posXEnd + posXStart;
            g.transform.eulerAngles = transform.eulerAngles;

            if(transform.eulerAngles.y == 0)
            {
                transform.position = new Vector3(posXStart, transform.position.y, transform.position.z);
                g.transform.position = new Vector3(sizeX / 2f, transform.position.y - 0.12f, transform.position.z);
                g.transform.localScale = new Vector3(Mathf.Abs(posXEnd - posXStart) + 1f, g.transform.localScale.y, g.transform.localScale.z);
                transform.DOMoveX(posXEnd, Mathf.Abs(posXEnd - posXStart) / 2f / speedMove).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo).SetDelay(timeDelay);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, posXStart);
                g.transform.position = new Vector3(transform.position.x, transform.position.y - 0.12f, sizeX / 2f);
                //g.transform.localScale = new Vector3(g.transform.localScale.z, g.transform.localScale.y, Mathf.Abs(posXEnd - posXStart) + 1f);
                g.transform.localScale = new Vector3(Mathf.Abs(posXEnd - posXStart) + 1f, g.transform.localScale.y, g.transform.localScale.z);
                transform.DOMoveZ(posXEnd, Mathf.Abs(posXEnd - posXStart) / 2f / speedMove).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo).SetDelay(timeDelay);
            }
        }
        //
        if (isRotate)
            transform.Rotate(dir * speedRotate);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelShop : PanelBase
{
    public Image imgSkinMain;
    public GameObject prefabButtonSkin;
    public Button btnClose;
    public Button btnUnlockRandom;
    public Transform content;
    private void Start()
    {
        btnClose.onClick.AddListener(() =>
        {
            gameObject.SetActive(false);
        });
        btnUnlockRandom.onClick.AddListener(ClickUnlockRandom);
        InitUISkin();
        
    }
    public void InitUISkin()
    {
        for (int i = 0; i < content.childCount; i++)
        {
            Destroy(content.GetChild(i).gameObject);
        }
        //
        for (int i = 0; i < ResourceManager.I.skinDTOs.Count; i++)
        {
            GameObject g = Instantiate(prefabButtonSkin);
            g.transform.SetParent(content);
            g.transform.localScale = Vector3.one;
            var sc = g.GetComponent<ButtonSkin>();
            sc.id = ResourceManager.I.skinDTOs[i].idSkin;
            sc.imgSkin.sprite = ResourceManager.I.skinDTOs[i].sprSkin;
            if (DataManager.I.GameData.isUnlockSkins[i])
            {
                sc.imgSkin.color = new Color(1, 1, 1);
                g.GetComponent<Button>().interactable = true;
            }
            else
            {
                sc.imgSkin.color = new Color(0, 0, 0);
                g.GetComponent<Button>().interactable = false;
            }
        }
        //
        SelectSkinByID(DataManager.I.GameData.idSkin);
        //
        if (IsUnlockAll() || DataManager.I.GameData.money < GameConfig.MONEY_UNLOCK_RANDOM)
        {
            btnUnlockRandom.interactable = false;
        }
        else
            btnUnlockRandom.interactable = true;
    }
    public void SelectSkinByID(int id)
    {
        for (int i = 0; i < content.childCount; i++)
        {
            if (content.GetChild(i).GetComponent<ButtonSkin>().id == id)
            {
                content.GetChild(i).GetComponent<Button>().interactable = true;
                content.GetChild(i).GetComponent<ButtonSkin>().imgBoder.SetActive(true);
                imgSkinMain.sprite = content.GetChild(i).GetComponent<ButtonSkin>().imgSkin.sprite;
                content.GetChild(i).GetComponent<ButtonSkin>().imgSkin.color = new Color(1, 1, 1);
                GameManager.I.disc.ChangeSkin(id);
            }
            else
                content.GetChild(i).GetComponent<ButtonSkin>().imgBoder.SetActive(false);
        }
    }
    public List<int> t;
    public void ClickUnlockRandom()
    {
        if (IsUnlockAll() || DataManager.I.GameData.money < GameConfig.MONEY_UNLOCK_RANDOM)
        {
            btnUnlockRandom.interactable = false;
            return;
        }
        else
        {
            t.Clear();
            for (int i = 0; i < DataManager.I.GameData.isUnlockSkins.Count; i++)
            {
                if (!DataManager.I.GameData.isUnlockSkins[i])
                    t.Add(i);
            }
            t.Shuffle();
            StartCoroutine(IEFocus());
        }
    }
    IEnumerator IEFocus()
    {
        btnUnlockRandom.interactable = false;
        btnClose.interactable = false;
        for (int i = 0; i < t.Count; i++)
        {
            FocusSkinRandom(t[i]);
            yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));
        }
        var r = t[Random.Range(0, t.Count - 1)];
        SelectSkinByID(r);
        //
        DataManager.I.GameData.money -= GameConfig.MONEY_UNLOCK_RANDOM;
        //
        DataManager.I.GameData.idSkin = r;
        DataManager.I.GameData.isUnlockSkins[r] = true;
        DataManager.I.SaveData();
        ManagerUI.I.DisplayMoney();
        btnUnlockRandom.interactable = true;
        btnClose.interactable = true;

        //
        if (IsUnlockAll() || DataManager.I.GameData.money < GameConfig.MONEY_UNLOCK_RANDOM)
        {
            btnUnlockRandom.interactable = false;
        }
        else
            btnUnlockRandom.interactable = true;
    }
    public void FocusSkinRandom(int id)
    {
        for (int i = 0; i < content.childCount; i++)
        {
            if (content.GetChild(i).GetComponent<ButtonSkin>().id == id)
            {
                content.GetChild(i).GetComponent<ButtonSkin>().imgBoder.SetActive(true);
            }
            else
                content.GetChild(i).GetComponent<ButtonSkin>().imgBoder.SetActive(false);
        }
    }
    public bool IsUnlockAll()
    {
        for (int i = 0; i < DataManager.I.GameData.isUnlockSkins.Count; i++)
        {
            if (!DataManager.I.GameData.isUnlockSkins[i])
                return false;
        }
        return true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PanelMain : PanelBase
{
    [SerializeField] private Button btnSetting;
    [SerializeField] private Button btnShop;
    [SerializeField] private Button m_btnSound;
    [SerializeField] private Button m_btnVibrate;
    [SerializeField] private Sprite m_spriteSoundOn;
    [SerializeField] private Sprite m_spriteSoundOff;
    [SerializeField] private Sprite m_spriteVibrationOn;
    [SerializeField] private Sprite m_spriteVibrationOff;
    public bool m_isShowSetting;
    Sequence m_mySeq;
    private void Start()
    {
        btnSetting.onClick.AddListener(FuncButtonSetting);
        m_btnSound.onClick.AddListener(FuncButtonSound);
        m_btnVibrate.onClick.AddListener(FuncButtonVibrate);
        btnShop.onClick.AddListener(()=>
        {
            ManagerUI.I.Show(TypePanelUI.PanelShop);
            ManagerUI.I.GetPanel<PanelShop>(TypePanelUI.PanelShop).InitUISkin();
        });
        HideAllSetting();
        ToggleSound();
        ToggleVibration();
    }
    public void FuncButtonSetting()
    {
        if (m_isShowSetting == false)
        {
            m_isShowSetting = true;
            m_mySeq.Kill();
            m_mySeq = DOTween.Sequence();
            m_btnSound.gameObject.SetActive(true);
            m_btnVibrate.gameObject.SetActive(true);
            //m_btnRestone.gameObject.SetActive(true);

            m_btnSound.gameObject.transform.localScale = Vector3.zero;
            m_btnVibrate.gameObject.transform.localScale = Vector3.zero;
            //m_btnRestone.gameObject.transform.localScale = Vector3.zero;

            float mTimeAnim = 0.25f;
            m_mySeq.Join(m_btnSound.gameObject.transform.DOScale(Vector3.one, mTimeAnim).SetEase(Ease.OutBack).SetDelay(0.05f));
            m_mySeq.Join(m_btnVibrate.gameObject.transform.DOScale(Vector3.one, mTimeAnim).SetEase(Ease.OutBack).SetDelay(0.075f));
            //m_mySeq.Join(m_btnRestone.gameObject.transform.DOScale(Vector3.one, mTimeAnim).SetEase(Ease.OutBack).SetDelay(0.1f));
            ToggleSound();
            ToggleVibration();
        }
        else
        {
            HideAllSetting();
        }
    }
    void ToggleSound()
    {
        if (DataManager.I.GameData.IsSound)
        {
            m_btnSound.GetComponent<Image>().sprite = m_spriteSoundOn;
        }
        else
        {
            m_btnSound.GetComponent<Image>().sprite = m_spriteSoundOff;
        }
    }
    public void FuncButtonSound()
    {
        if (DataManager.I.GameData.IsSound)
            DataManager.I.GameData.IsSound = false;
        else
            DataManager.I.GameData.IsSound = true;
        ToggleSound();
        //SoundManager.Instance.ToggleSound();
        DataManager.I.SaveData();
    }

    void ToggleVibration()
    {
        if (DataManager.I.GameData.IsHapic)
        {
            m_btnVibrate.GetComponent<Image>().sprite = m_spriteVibrationOn;
        }
        else
        {
            m_btnVibrate.GetComponent<Image>().sprite =m_spriteVibrationOff;
        }
    }

    public void FuncButtonVibrate()
    {
        if (DataManager.I.GameData.IsHapic)
            DataManager.I.GameData.IsHapic = false;
        else
            DataManager.I.GameData.IsHapic = true;
        ToggleVibration();
        DataManager.I.SaveData();
    }
    public void CheckCloseSetting()
    {
        if (m_isShowSetting)
        {
            HideAllSetting();
        }
    }

    public void HideAllSetting()
    {
        if (m_isShowSetting == false)
            return;
        m_isShowSetting = false;
        m_btnSound.gameObject.SetActive(false);
        m_btnVibrate.gameObject.SetActive(false);

    }
}

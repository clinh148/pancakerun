﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using Game;

public class PanelGameWin : PanelBase
{
    [SerializeField] private Button _btnNext;
    [SerializeField] private Button _btnReward;
    [SerializeField] private TextMeshProUGUI tmpMoneyCollect;
    private void OnEnable()
    {
        _btnReward.gameObject.SetActive(true);
        tmpMoneyCollect.text = GameManager.I.moneyCollect.ToString();
        _btnNext.gameObject.SetActive(false);
        _btnNext.transform.localScale = Vector3.zero;
        _btnNext.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "No, thanks";
        Run.After(2f, () =>
        {
            _btnNext.gameObject.SetActive(true);
            _btnNext.transform.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBack);
        });
    }
    private void Start()
    {
        _btnNext.onClick.AddListener(ClickNext);
        _btnReward.onClick.AddListener(()=>
        {
#if UNITY_EDITOR
            DoneReward(true);
            //Game.AdsManager.ShowVideo(DoneReward);
#else
    Game.AdsManager.ShowVideo(DoneReward);
#endif

        });
    }
    private void DoneReward(bool isSuccess)
    {
        if (isSuccess)
        {
            //_btnReward.interactable = false;
            _btnReward.gameObject.SetActive(false);
            tmpMoneyCollect.text = (GameManager.I.moneyCollect * 2).ToString();
            DataManager.I.GameData.money += GameManager.I.moneyCollect;
            DataManager.I.SaveData();
            ManagerUI.I.DisplayMoney();
            _btnNext.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Next level";
            ManagerUI.I.timeShowInter = System.DateTime.Now;
        }
        else
        {
            Debug.Log("no reward");
        }
    }
    private void ClickNext()
    {
        GameManager.I.Restart();
    }
   
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelGameOver : PanelBase
{
    [SerializeField] private Button _btnRetry;
    private void Start()
    {
        _btnRetry.onClick.AddListener(ClickRetry);
    }
    private void ClickRetry()
    {
        GameManager.I.Restart();
    }
}

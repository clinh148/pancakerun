﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class GamePlayControl : Singleton<GamePlayControl>
{

    private bool _isActive =false;

    public void Init()
    {

    }

    public void Active()
    {
        _isActive = true;
    }

    private void Update()
    {
        if (_isActive)
        {

        }
    }
}

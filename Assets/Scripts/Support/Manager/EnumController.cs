﻿using System;

public enum TypePool
{
   effFood = 0,
   effWater,
   effSpawn,
}
public enum TypeSound
{
    win = 0,
    lose,
    collect,
    obs,
    finish,
    stairstep,
    maneat,
}

public enum TypePanelUI
{
    PanelMain,
    PanelGameOver,
    PanelGameWin,
    PanelInGame,
    PanelPause,
    PanelSetting,
    PanelShop,
    PanelTutorial,
    PanelSession,
}

public enum TypeStateGame
{
    None,
    Lobby,
    FindingOpponent,
    Playing,
    GameOver,
    GameWin,
    Draw,
    Finish
}

public class EnumController
{
    public static TypePool ConvertToPool(string type)
    {
        return (TypePool)Enum.Parse(typeof(TypePool), type, false);
    }
}
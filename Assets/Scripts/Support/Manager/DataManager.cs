﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : Singleton<DataManager>
{
    private const string KEY_DATA = "DataGame_";

    [SerializeField]
    private GameData _gameData = new GameData();

    public GameData GameData
    {
        get { return _gameData; }
        set { _gameData = value; }
    }

    protected override void Awake()
    {
        base.Awake();
        InitData();
    }

    public void InitData()
    {
        _gameData = JsonUtility.FromJson<GameData>(PlayerPrefs.GetString(KEY_DATA));
        if (_gameData == null)
        {
            _gameData = new GameData();
            for (int i = 0; i < ResourceManager.I.skinDTOs.Count; i++)
            {
                _gameData.isUnlockSkins.Add(ResourceManager.I.skinDTOs[i].isUnlock);
            }
            SaveData();
        }
    }
    [ContextMenu("SaveData")]
    public void SaveData()
    {
        PlayerPrefs.SetString(KEY_DATA, JsonUtility.ToJson(_gameData));
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
    }
}
[System.Serializable]
public class GameData
{
    [Header("Main Data")]
    public int level;
    public bool IsSound;
    public bool IsHapic;
    public int idSkin;
    public long money;
    public List<bool> isUnlockSkins;

    public GameData()
    {
        level = 0;
        IsSound = true;
        IsHapic = true;
        idSkin = 0;
        money = 0;
        isUnlockSkins = new List<bool>();
    }
}

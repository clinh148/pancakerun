﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public TypeSound _typeSound;
    public AudioClip _audioClip;
}
public class SoundManager : Singleton<SoundManager>
{
    //public AudioSource _musicSource;
    public AudioSource _soundSource;
    public List<Sound> _sounds;
    private void Start()
    {
        if (_soundSource)
            _soundSource = GetComponent<AudioSource>();
    }
    public void PlaySound(TypeSound typeSound, bool isLoop = false)
    {
        if (DataManager.I.GameData.IsSound)
        {
            for (int i = 0; i < _sounds.Count; i++)
            {
                if (_sounds[i]._typeSound == typeSound)
                {
                    _soundSource.clip = _sounds[i]._audioClip;
                    _soundSource.loop = isLoop;
                    _soundSource.volume = 1f;
                    _soundSource.time = 0;
                    _soundSource.Play();
                }
            }
        }
    }
    public void PlaySoundRap(TypeSound typeSound, float time = 0)
    {
        if (DataManager.I.GameData.IsSound)
        {
            for (int i = 0; i < _sounds.Count; i++)
            {
                if (_sounds[i]._typeSound == typeSound)
                {
                    _soundSource.clip = _sounds[i]._audioClip;
                    _soundSource.time = time;
                    _soundSource.volume = 1f;
                    _soundSource.Play();
                }
            }
        }
    }
    public void StopSound()
    {
        _soundSource.Stop();
    }
    public void PauseSound()
    {
        _soundSource.Pause();
    }
    /*public void PlayMusic(TypeSound typeSound, bool isLoop = true)
    {
        if (DataManager.I.GameData.IsSound)
        {
            for (int i = 0; i < _sounds.Count; i++)
            {
                if (_sounds[i]._typeSound == typeSound)
                {
                    _musicSource.clip = _sounds[i]._audioClip;
                    _musicSource.loop = isLoop;
                    _musicSource.Play();
                }
            }
        }
    }
    public void StopMusic()
    {
        _musicSource.Stop();
    }*/
}

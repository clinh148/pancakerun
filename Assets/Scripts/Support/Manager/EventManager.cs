﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventManager
{
    //Delegate
    public delegate void OnUpdateScore();
    public delegate void OnUpdateDiamond();
    public delegate void OnSkinChange();

    //Event
    public static event OnUpdateScore _onUpdateScore;
    public static event OnUpdateDiamond _onUpdateDiamond;
    public static event OnSkinChange _onSkinChange;

    public static void UpdateScore()
    {
        _onUpdateScore?.Invoke();
    }


    public static void ChangeSkin()
    {
        _onSkinChange?.Invoke();
    }


    public static void UpdateDiamond()
    {
        _onUpdateDiamond?.Invoke();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour
{
    //[SerializeField] private Transform _target=default;
    [SerializeField] private float _valueLerp=default;
    //private Vector3 offset;
    /*public void SetTargert(Transform target)
    {
        target = _target;
    }*/
    private void Start()
    {
        //offset = transform.position - GameManager.I.pathFollower.pathCreator.path.GetPointAtDistance(GameManager.I.pathFollower.distanceTravelled);
    }
    private void LateUpdate()
    {
        Vector3 pos = GameManager.I.pathFollower.pathCreator.path.GetPointAtDistance(GameManager.I.pathFollower.distanceTravelled - GameConfig.DIS_CAM, GameManager.I.pathFollower.endOfPathInstruction);
        pos.y += 5.5f;
        //Camera.main.fieldOfView += GameManager.I.disc._stacks.Count * 0.25f;
        //pos.y += (GameManager.I.disc._stacks.Count / 20) + 5.5f;
        //transform.position = Vector3.Lerp(transform.position,pos, 0.5f);
        transform.position = pos;
        transform.rotation = GameManager.I.pathFollower.pathCreator.path.GetRotationAtDistance(GameManager.I.pathFollower.distanceTravelled-2, GameManager.I.pathFollower.endOfPathInstruction);
        //transform.position = new Vector3(transform.position.x, _target.position.y + 6, _target.position.z - 9);
        //var x = 10 + (GameManager.I.disc._stacks.Count / 10);
        transform.eulerAngles = new Vector3(10 - ((Camera.main.fieldOfView - 80) / 2f) /*+ (GameManager.I.pathFollower.transform.eulerAngles.x / 2)*/,transform.eulerAngles.y,transform.eulerAngles.z);
        //Debug.Log(GameManager.I.pathFollower.transform.localEulerAngles.x);
    }
}

﻿using UnityEngine;
using DG.Tweening;

public class SmoothFollow : MonoBehaviour
{
    public Transform target = default;
    private Vector3 offsetTarget = default;
    private Vector3 rotTarget = default;
   
    private void Start()
    {
        offsetTarget = transform.position - target.position;
        rotTarget = transform.eulerAngles;
    }
    void LateUpdate()
    {
        if (!target)
            return;
        //transform.eulerAngles =new Vector3(transform.eulerAngles.x,target.eulerAngles.y,transform.eulerAngles.z);
        transform.position = new Vector3(/*target.position.x + offsetTarget.x*/transform.position.x,
            target.position.y + offsetTarget.y, target.position.z + offsetTarget.z);

    }
    public void SetTarget(Transform target)
    {
       if (target == null)
            return;
        this.target = target;
        offsetTarget = transform.position - target.position;
        //offsetTarget.y = GameManager.I.PosYCamera();
        rotTarget = transform.eulerAngles;
    }
}
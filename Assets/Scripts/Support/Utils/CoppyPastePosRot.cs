﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CoppyPastePosRot : MonoBehaviour
{

    public  Vector3 pos;
    public  Quaternion rot;
    

    [ContextMenu("Coppy")]
    public void Coppy()
    {
        pos = transform.localPosition;
        rot = transform.localRotation;
    }


    [ContextMenu("Paste")]
    public void Paste()
    {
        transform.localPosition = pos;
        transform.localRotation = rot;
    }
}

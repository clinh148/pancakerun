﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : Singleton<GameMaster>
{
    public TypeStateGame TypeStateGame { get; set; }

    private void Start()
    {
        Application.targetFrameRate = 60;
        InitGame();
    }

    public void InitGame()
    {
        TypeStateGame = TypeStateGame.Lobby;
        GamePlayControl.I.Init();
        Debug.Log("Init Game");
    }

    public void StartGame()
    {
        TypeStateGame = TypeStateGame.Playing;
        GamePlayControl.I.Active();
        Debug.Log("Start Game");
    }

    public void GameWin()
    {
        if (TypeStateGame != TypeStateGame.Playing)
            return;
        TypeStateGame = TypeStateGame.GameWin;
        Debug.Log("Game Win");
    }

    public void GameOver()
    {
        if (TypeStateGame != TypeStateGame.Playing)
            return;
        TypeStateGame = TypeStateGame.GameOver;
        Debug.Log("Game Over");
    }

    public void ReloadGame()
    {
        PoolController.I.HideAllPool();
        UIAllGame.I.LoadScene(0);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ManagerUI : Singleton<ManagerUI>, ManagerIUI
{
    [SerializeField] private Image _panelLoading;
    [SerializeField] private TextMeshProUGUI tmpLevel;
    public TextMeshProUGUI tmpMoney;
    public GameObject objCurrency;
    [SerializeField] private List<PanelDTO> _panelDTOs = new List<PanelDTO>();
    private Dictionary<TypePanelUI,PanelBase> _mapper = new Dictionary<TypePanelUI, PanelBase>();
    //[HideInInspector]
    public System.DateTime timeShowInter;
    protected override void Awake()
    {
        base.Awake();
        Init();
    }
    private void Start()
    {
        timeShowInter = System.DateTime.Now;
        DisplayMoney();
    }
    void OnValidate()
    {
        for (int i = 0; i < _panelDTOs.Count; i++)
        {
            _panelDTOs[i].name = _panelDTOs[i].type.ToString();
        }
    }

    private void Init()
    {
        for (int i = 0; i < _panelDTOs.Count; i++)
        {
            if (_mapper.ContainsKey(_panelDTOs[i].type) == false)
            {
                _mapper.Add(_panelDTOs[i].type, _panelDTOs[i].panel);
            }
            else Debug.LogWarning(_panelDTOs[i].type + "has been register !");
        }
    }

    public void Hide(TypePanelUI type, System.Action callBack = null)
    {
        if (_mapper.ContainsKey(type))
            _mapper[type].DeActiveMe(callBack);
        else Debug.LogWarning(type + "not register");
    }

    public void Show(TypePanelUI type, System.Action callBack = null)
    {
        if (_mapper.ContainsKey(type))
            _mapper[type].ActiveMe(callBack);
        else Debug.LogWarning(type + " not register");
    }
    public void LoadLevel()
    {
        Hide(TypePanelUI.PanelGameOver);
        Hide(TypePanelUI.PanelGameWin);
        Loading(LoadingStart);
    }
    public void LoadingStart()
    {
        //Debug.Log(UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings);
        int id = DataManager.I.GameData.level % (UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings - 1);
        UnityEngine.SceneManagement.SceneManager.LoadScene(id + 1);
        DisplayLevel(DataManager.I.GameData.level + 1);
        Hide(TypePanelUI.PanelInGame);
        Hide(TypePanelUI.PanelGameOver);
        Hide(TypePanelUI.PanelGameWin);
        Show(TypePanelUI.PanelMain);
    }
    public T GetPanel<T>(TypePanelUI type) where T : PanelBase
    {
        return (T)_mapper[type];
    }
    public void DisplayLevel(int level)
    {
        tmpLevel.gameObject.SetActive(true);
        tmpLevel.text = "Level " + level.ToString();
    }   
    public void DisplayMoney()
    {
        //objCurrency.SetActive(isActive);
        tmpMoney.text = DataManager.I.GameData.money.ToString();
        //tmpMoney.text = FormatMoneyUnit((long) DataManager.I.GameData.money);
    }
    public static string FormatMoneyUnit(long value)
    {
        string str = "";
        if (value < 1000) str = value.ToString();
        else if (value < 1000000)
        {
            str = System.Math.Round((value / 1000f), 2) + "K";
        }
        else if (value < 1000000000)
        {
            str = System.Math.Round((value / 1000000f), 2) + "M";
        }
        else str = System.Math.Round((value / 1000000000f), 2) + "B";
        return str;
    }
    public void Loading(System.Action callBack1/*, System.Action callBack2 = null*/)
    {
        _panelLoading.gameObject.SetActive(true);
        _panelLoading.DOFade(0F, 0F);
        Tweener t1 = _panelLoading.DOFade(1f, 0.2f);
        t1.OnComplete(() =>
        {
            callBack1();
            Tweener t2 = _panelLoading.DOFade(0f, 0.2f);
            t2.OnComplete(() =>
            {
                //callBack2();
                _panelLoading.gameObject.SetActive(false);
            });
        });
    }

}

[System.Serializable]
public class PanelDTO
{
    [HideInInspector] public string name;
    public TypePanelUI type;
    public PanelBase panel;
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class UIAllGame : Singleton<UIAllGame>
{
    [SerializeField] private Image _loadingScene=default;


    public void LoadScene(int id)
    {
        _loadingScene.gameObject.SetActive(true);
        Color color = _loadingScene.color;
        color.a = 0f;
        _loadingScene.DOFade(1f, 0.5f);

        AsyncOperation async = SceneManager.LoadSceneAsync(id);
        //async.allowSceneActivation = false;

        _loadingScene.DOFade(0f, 0.5f)
            .SetDelay(1.5f)
            .OnComplete(() =>
            {
                _loadingScene.gameObject.SetActive(false);
                //async.allowSceneActivation = true;
            });
    }
}

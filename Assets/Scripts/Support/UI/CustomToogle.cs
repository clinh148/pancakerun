﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CustomToogle : MonoBehaviour
{
    [SerializeField] private TypeToogle _type= default;
    [SerializeField] RectTransform _scrollRect=default;
    [SerializeField] private float _deltaScroll=1f;
    [SerializeField] private float _timeScroll=0.5f;

    private Vector3 _initPos;
    private bool _isOn;

    private void Start()
    {
        _initPos = _scrollRect.anchoredPosition3D;

        switch (_type)
        {
            case TypeToogle.ToogleHapic:
                _isOn = DataManager.I.GameData.IsHapic;
                break;
            case TypeToogle.ToogleSound:
                _isOn = DataManager.I.GameData.IsSound;
                break;
        }

        if (!_isOn)
        {
            _scrollRect.DOAnchorPos3DX(_initPos.x - _deltaScroll, 0f);
        }
        else
        {
            _scrollRect.DOAnchorPos3DX(_initPos.x, 0f);
        }
    }

    public void ClickMe()
    {
        if (_isOn)
        {
            _scrollRect.DOAnchorPos3DX(_initPos.x - _deltaScroll, _timeScroll);
        }
        else
        {
            _scrollRect.DOAnchorPos3DX(_initPos.x, _timeScroll);
        }
        _isOn = !_isOn;

        switch (_type)
        {
            case TypeToogle.ToogleHapic:
                DataManager.I.GameData.IsHapic = _isOn;
                break;
            case TypeToogle.ToogleSound:
                DataManager.I.GameData.IsSound = _isOn;
                break;
        }
        DataManager.I.SaveData();
    }
}

public enum TypeToogle
{
    ToogleSound,
    ToogleHapic
}
﻿
public interface ManagerIUI
{
    void Show(TypePanelUI type,System.Action callBack);
    void Hide(TypePanelUI type,System.Action callBack);
}
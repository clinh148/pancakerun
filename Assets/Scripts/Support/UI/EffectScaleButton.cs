﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EffectScaleButton : MonoBehaviour
{
    [SerializeField] private Vector3 _valueScale=new Vector3(1.05f,1.05f,1.05f);
    [SerializeField] private float _time=1f;

    private void Start()
    {
        Tweener t1= transform.DOScale(_valueScale, _time);
        t1.SetEase(Ease.Linear);
        t1.OnComplete(() =>
        {
            if (gameObject.activeSelf)
            {
                Tweener t2= transform.DOScale(Vector3.one, _time);
                t2.SetEase(Ease.Linear);
                t2.OnComplete(() =>
                {
                    if (gameObject.activeSelf)
                        Start();
                });
            }
        });
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EffectShakeButton : MonoBehaviour
{
    [SerializeField] private Vector3 _shakingVector=default;
    [SerializeField] private float _duration;
    void Start()
    {
        Tweener t1 = transform.DOShakeRotation(0.15f, _shakingVector);
        t1.SetLoops(3);
        t1.SetDelay(Random.Range(0, 0.5f));
        t1.OnComplete(() =>
        {
            if (gameObject.activeSelf)
                Start();
        });
    }

   
}

using System;

namespace Game
{
    public interface IAdsManager
    {
        void ShowInterstitial(Action onClosed);
        void ShowVideo(Action<bool> onReward);
        void ShowBanner();
        void HideBanner();
        bool IsVideoReady();
        bool IsInterstitialReady();
    }
}
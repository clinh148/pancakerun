#define IRONSOURCE

#if IRONSOURCE

using System;
using DG.Tweening;
using UnityEngine;

namespace Game
{
    public class AdsIronSource : IAdsManager
    {
        static readonly float ReloadAdsInterval = 5f;
        static readonly int ReloadAdsMaxAttemp = 5;

#if UNITY_ANDROID
        static readonly string ID = "114f93e3d";
#else
        static readonly string ID = "114f93e3d";
#endif

        Tween _interstitialTween;
        int _interstitialAttemp;
        Tween _rewardedTween;

        bool _videoRewarded = false;

        Action<bool> _onVideoRewarded;
        Action _onInterstitialClosed;

        #region Construct

        public AdsIronSource()
        {
            IronSource.Agent.init(ID);
            IronSource.Agent.validateIntegration();

            IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
            IronSource.Agent.loadInterstitial();

            IronSourceEvents.onRewardedVideoAdRewardedEvent += IronSourceEvents_onRewardedVideoAdRewardedEvent;
            IronSourceEvents.onRewardedVideoAdClosedEvent += IronSourceEvents_onRewardedVideoAdClosedEvent;
            IronSourceEvents.onRewardedVideoAdShowFailedEvent += IronSourceEvents_onRewardedVideoAdShowFailedEvent;

            IronSourceEvents.onInterstitialAdClosedEvent += IronSourceEvents_onInterstitialAdClosedEvent;
            IronSourceEvents.onInterstitialAdShowFailedEvent += IronSourceEvents_onInterstitialAdShowFailedEvent;
            IronSourceEvents.onInterstitialAdLoadFailedEvent += IronSourceEvents_onInterstitialAdLoadFailedEvent;
            IronSourceEvents.onInterstitialAdReadyEvent += IronSourceEvents_onInterstitialAdReadyEvent;

            IronSourceEvents.onBannerAdLoadFailedEvent += IronSourceEvents_onBannerAdLoadFailedEvent;
            IronSourceEvents.onBannerAdLoadedEvent += IronSourceEvents_onBannerAdLoadedEvent;

            GameMaster.OnGamePaused += PGameMaster_OnGamePaused;
        }

        void IronSourceEvents_onInterstitialAdReadyEvent()
        {
            Debug.Log("Interstitial loaded");

            _interstitialAttemp = 0;
        }

        #endregion

        void PGameMaster_OnGamePaused(bool paused)
        {
            IronSource.Agent.onApplicationPause(paused);
        }

        void IronSourceEvents_onInterstitialAdClosedEvent()
        {
            _onInterstitialClosed?.Invoke();

            IronSource.Agent.loadInterstitial();
        }

        void IronSourceEvents_onInterstitialAdLoadFailedEvent(IronSourceError obj)
        {
            Debug.LogFormat("Interstitial failed to load: {0}", obj.getDescription());

            if (_interstitialAttemp > ReloadAdsMaxAttemp)
                return;

            _interstitialAttemp++;

            _interstitialTween?.Kill();
            _interstitialTween = DOVirtual.DelayedCall(ReloadAdsInterval * _interstitialAttemp, () =>
            {
                if (!IronSource.Agent.isInterstitialReady())
                    IronSource.Agent.loadInterstitial();
            });
        }

        void IronSourceEvents_onInterstitialAdShowFailedEvent(IronSourceError obj)
        {
            _onInterstitialClosed?.Invoke();

            Debug.LogFormat("Interstitial failed to show: {0}", obj.getDescription());
        }

        void IronSourceEvents_onRewardedVideoAdShowFailedEvent(IronSourceError obj)
        {
            _onVideoRewarded?.Invoke(_videoRewarded);

            Debug.Log(obj);
        }

        void IronSourceEvents_onRewardedVideoAdRewardedEvent(IronSourcePlacement obj)
        {
            _videoRewarded = true;
        }

        void IronSourceEvents_onRewardedVideoAdClosedEvent()
        {
            // Because the onRewardedVideoAdRewardedEvent and onRewardedVideoAdClosedEvent are asynchronous
            _rewardedTween?.Kill();
            _rewardedTween = DOVirtual.DelayedCall(0.5f, () => { _onVideoRewarded?.Invoke(_videoRewarded); });
        }

        #region IADManager

        void IAdsManager.ShowInterstitial(Action onClosed)
        {
            _onInterstitialClosed = onClosed;

            if (IronSource.Agent.isInterstitialReady())
                IronSource.Agent.showInterstitial();
            else
                _onInterstitialClosed?.Invoke();
        }

        void IAdsManager.ShowVideo(Action<bool> onReward)
        {
            _videoRewarded = false;

            _onVideoRewarded = onReward;

            if (IronSource.Agent.isRewardedVideoAvailable())
                IronSource.Agent.showRewardedVideo();
            else
                _onVideoRewarded?.Invoke(false);
        }

        void IAdsManager.ShowBanner()
        {
            IronSource.Agent.displayBanner();
        }

        void IAdsManager.HideBanner()
        {
            IronSource.Agent.hideBanner();
        }

        bool IAdsManager.IsVideoReady()
        {
            return IronSource.Agent.isRewardedVideoAvailable();
        }

        bool IAdsManager.IsInterstitialReady()
        {
            return IronSource.Agent.isInterstitialReady();
        }

        #endregion

        void IronSourceEvents_onBannerAdLoadedEvent()
        {
        }

        void IronSourceEvents_onBannerAdLoadFailedEvent(IronSourceError obj)
        {
            Debug.Log(obj);
        }

        /*

        public static Vector2 GetBannerSizePixel()
        {
#if UNITY_IOS

            return (Vector2)GetBannerSize();

#elif UNITY_ANDROID && !UNITY_EDITOR

            Vector2 bannerSize = GetBannerSize();

            return new Vector2(bannerSize.x * (DisplayMetricsAndroid.DPI / 160f), bannerSize.y * (DisplayMetricsAndroid.DPI / 160f));

#else

            return Vector2.zero;

#endif
        }

        static Vector2Int GetBannerSize()
        {
#if UNITY_IOS

            if (UnityEngine.iOS.Device.generation.ToString().IndexOf("iPad") > -1)
                return new Vector2Int(728, 90);
            else
                return new Vector2Int(320, 50);

#elif UNITY_ANDROID && !UNITY_EDITOR

            float actualHeight = Screen.height / DisplayMetricsAndroid.DPI;
            float dp = actualHeight * 160f;

            if (dp > 720f)
                return new Vector2Int(728, 90);
            else
                return new Vector2Int(320, 50);

#else

            return Vector2Int.zero;

#endif
        }
        */
    }
}

#endif
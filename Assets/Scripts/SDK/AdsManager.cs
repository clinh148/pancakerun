#if UNITY_EDITOR
#define ADS
#endif

#define IRONSOURCE

using System;
using UnityEngine;

namespace Game
{
    public static class AdsManager
    {
        static IAdsManager _adsManager;

        public static event Action<bool> OnVideoRewarded;
        public static event Action OnInterstitalClosed;

        #region Runtime Init

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        static void RuntimeInit()
        {
#if ADS && IRONSOURCE
            _adsManager = new AdsIronSource();
#elif ADS && MAX
            _adsManager = new AdsMAX();
#endif
        }

        #endregion

        public static void ShowVideo(Action<bool> onReward)
        {
#if ADS
            _adsManager.ShowVideo((reward) =>
            {
                onReward?.Invoke(reward);
                OnVideoRewarded?.Invoke(reward);
            });
#else
            onReward?.Invoke(true);
            OnVideoRewarded?.Invoke(true);
#endif
        }

        public static void ShowInterstitial(Action onClosed)
        {
#if ADS
            _adsManager.ShowInterstitial(() =>
            {
                onClosed?.Invoke();
                OnInterstitalClosed?.Invoke();
            });
#else
            onClosed?.Invoke();
            OnInterstitalClosed?.Invoke();
#endif
        }

        public static bool IsVideoReady()
        {
#if ADS
            return _adsManager.IsVideoReady();
#else
            return true;
#endif
        }

        public static bool IsInterstitialReady()
        {
#if ADS
            return _adsManager.IsInterstitialReady();
#else
            return false;
#endif
        }

        public static void ShowBanner()
        {
#if ADS
            _adsManager.ShowBanner();
#endif
        }

        public static void HideBanner()
        {
#if ADS
            _adsManager.HideBanner();
#endif
        }
    }
}
﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class GameMaster : MonoBehaviour
    {
        public static event Action<bool> OnGamePaused;
        public static event Action OnGameQuit;
        public static event Action OnSceneChanged;

        #region Runtime Init

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        static void Init()
        {
            GameObject obj = new GameObject(typeof(GameMaster).ToString());
            obj.AddComponent<GameMaster>();
        }

        #endregion

        #region MonoBehaviour

        void Awake()
        {
#if UNITY_IOS
            if (UnityEngine.iOS.Device.lowPowerModeEnabled)
                Application.targetFrameRate = 30;
            else
                Application.targetFrameRate = 60;
#else
            Application.targetFrameRate = 60;
#endif

            DontDestroyOnLoad(gameObject);
            InitFirebase();

            SceneManager.activeSceneChanged += SceneManager_ActiveSceneChanged;
        }

        void OnApplicationPause(bool pause)
        {
            OnGamePaused?.Invoke(pause);
        }

        void OnApplicationQuit()
        {
            OnGameQuit?.Invoke();
        }

        #endregion

        void InitFirebase()
        {
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    var app = Firebase.FirebaseApp.DefaultInstance;

                    // Set a flag here to indicate whether Firebase is ready to use by your app.
                }
                else
                {
                    UnityEngine.Debug.LogError(System.String.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });

        }

        void SceneManager_ActiveSceneChanged(Scene arg0, Scene arg1)
        {
            OnSceneChanged?.Invoke();
        }
    }
}
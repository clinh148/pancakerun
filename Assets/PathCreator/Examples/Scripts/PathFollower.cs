﻿using UnityEngine;

namespace PathCreation.Examples
{
    // Moves along a path at constant speed.
    // Depending on the end of path instruction, will either loop, reverse, or stop at the end of the path.
    public class PathFollower : MonoBehaviour
    {
        public PathCreator pathCreator;
        public EndOfPathInstruction endOfPathInstruction;
        public float speed = 0;
        public float distanceTravelled;
        public Transform model;
        private Vector3 posOld;
        private Vector3 delta;
        private Vector3 dir;
        private float smooth = 1.2f;
        private float valueClamp = 2.5f;
        public bool isControl = false;
        public bool isControlBonus = false;
        void Start()
        {
            isControl = true;
            distanceTravelled = 10;
            if (pathCreator != null)
            {
                // Subscribed to the pathUpdated event so that we're notified if the path changes during the game
                pathCreator.pathUpdated += OnPathChanged;
            }
        }

        void Update()
        {
            if (pathCreator != null && GameManager.I.stateGame == TypeStateGame.Playing)
            {
                /*distanceTravelled += speed * Time.deltaTime;
                transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
                transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);*/
                if(isControl)
                    Control();
                /*if (isControlBonus)
                {
                    ControlBonus();
                }*/
            }
        }
        public void Control()
        {
            if (Input.GetMouseButtonDown(0) && !UtilGame.CheckTouchUI())
            {
                posOld = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0) && !UtilGame.CheckTouchUI())
            {
                GameManager.I.PlayGame();

                delta = Input.mousePosition - posOld;
                posOld = Input.mousePosition;
                speed = GameConfig.SPEED_MOVE;

            }
            else if (Input.GetMouseButtonUp(0))
            {
                delta = Vector3.zero;
                dir = Vector3.zero;
                speed = 0;
            }
            //
            if (delta != Vector3.zero && delta.magnitude >= 0.2f)
            {
                dir.x = delta.x * Time.deltaTime * smooth;

                model.transform.localPosition += dir;
                if (model.transform.localPosition.x > 0f)       //right
                {
                    if (model.transform.localPosition.x > valueClamp)
                        model.transform.localPosition = new Vector3(valueClamp, model.transform.localPosition.y, 0f);
                }
                else    //left
                {
                    if (model.transform.localPosition.x < -valueClamp)
                        model.transform.localPosition = new Vector3(-valueClamp, model.transform.localPosition.y, 0f);
                }
            }
            //
            distanceTravelled += speed * Time.deltaTime;
            transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
            transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);
            //Debug.Log(pathCreator.path.GetNormalAtDistance(distanceTravelled, endOfPathInstruction));
        }

        public void ControlBonus()
        {
            if (Input.GetMouseButtonDown(0))
            {
                posOld = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                delta = Input.mousePosition - posOld;
                posOld = Input.mousePosition;
                //speed = GameConfig.SPEED_MOVE;

            }
            else if (Input.GetMouseButtonUp(0))
            {
                delta = Vector3.zero;
                dir = Vector3.zero;
                //speed = 0;
            }
            //
            if (delta != Vector3.zero)
            {
                dir.x = delta.x * Time.deltaTime * smooth;

                model.transform.localPosition += dir;
                if (model.transform.localPosition.x > 0f)       //right
                {
                    if (model.transform.localPosition.x > valueClamp)
                        model.transform.localPosition = new Vector3(valueClamp, model.transform.localPosition.y, model.transform.localPosition.z);
                }
                else    //left
                {
                    if (model.transform.localPosition.x < -valueClamp)
                        model.transform.localPosition = new Vector3(-valueClamp, model.transform.localPosition.y, model.transform.localPosition.z);
                }
            }
            //
            model.transform.Translate(GameManager.I.finish.objRoadBonus.transform.forward * Time.deltaTime * GameConfig.SPEED_MOVE);
        }
        // If the path changes during the game, update the distance travelled so that the follower's position on the new path
        // is as close as possible to its position on the old path
        void OnPathChanged()
        {
            distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(transform.position);
        }
    }
}